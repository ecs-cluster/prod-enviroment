# Terragrunt IAC ECS-Cluster + ALB + RDS | + Git pre-commit hook | Terraform/Terragrunt CI/CD Pipeline | prod-enviroment

## Terraform
```sh
Version ~> 1.6.6
```


## Terragrunt
```sh
Version ~> 0.54.5
```



## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |






## Owner
```sh
Volodymyr Hrytsaienko
```