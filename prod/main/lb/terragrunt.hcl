terraform {
  source = "${include.root.locals.source_url}//modules/main/lb?ref=${include.root.locals.source_version}"
}


include "root" {
  path   = find_in_parent_folders()
  expose = true
}

dependency "VPC" {
  config_path                             = "../networking/VPC/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id         = "fake-vpc-id"
    vpc_cidr_block = "10.0.0.0/16"
    Public_ids     = ["10.0.10.0/24", "10.0.16.0/24"]
  }
}

dependency "sg" {
  config_path                             = "../networking/S_Group/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    Securitygroup_id = "fake-Securitygroup-id"

  }
}

inputs = {
  security_groups  = dependency.sg.outputs.Securitygroup_id
  subnets          = dependency.VPC.outputs.Public_ids
  vpc_id           = dependency.VPC.outputs.vpc_id
  domain_name      = "example.com"
  domain_name_app2 = "example.com/app2"
  domain_name_app3 = "example.com/app3"
}
