terraform {
  source = "${include.root.locals.source_url}//modules/main/Logs?ref=${include.root.locals.source_version}"
}


include "root" {
  path   = find_in_parent_folders()
  expose = true
}


dependency "ECS" {
  config_path = "../ECS-Cluster/"
}
